/**
 * Created by Ronald on 12-11-14.
 */


function initializeGameObjects(){

    Q.Sprite.extend("Point", {
       init: function(p){
           this._super(p, {
               h: 30,
               w: 30,
              gravity: 0,
              scale: 0.5
           });
           this.add("2d");
       }
    });


    Q.Sprite.extend("Treasure", {
        init: function(p){
            this._super(p, {
                h: 30,
                w: 30,
               gravity: 0,
               scale: 2
            });
            this.add("2d")
        }
    });


    Q.Sprite.extend("SeaMonster", {
        init: function(p){
            this._super(p, {
                h: 30,
                w: 30,
                gravity: 0
            });
            this.add("2d")
        }
    });


    Q.Sprite.extend("Rock", {
        init: function(p){
            this._super(p, {
                h: 30,
                w: 30,
                gravity: 0
            });
            this.add("2d")
        }
    });


    Q.Sprite.extend("Waves", {
        init: function(p){
            this._super(p, {
                h: 30,
                w: 30,
                gravity: 0
            });
            this.add("2d")
        }
    });


    Q.Sprite.extend("Shore", {
        init: function(p){
            this._super(p, {
                h: 30,
                w: 30,
               gravity: 0,
               scale: 0.5
            });
            this.add("2d")
        }
    });

    Q.Sprite.extend("Test", {
        init: function(p){
            this._super(p, {
                h: 30,
                w: 30,
                gravity: 0,
                scale: 3
            });
        }
    });
}

