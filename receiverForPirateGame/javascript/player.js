/**
 * Created by Ronald on 12-11-14.
 */


function initializePlayer(){
    Q.Sprite.extend("Player", {
        init: function(p){
            this._super(p, {
                sheet: "player",
                sprite: "player",
                x: 500,
                y: 50,
                scale : 0.09,
                gravity: 0,
                angle: 90,
                direction: ""

            });
            this.add("2d, sensorControls, animation");
            this.on("hit", function(col){
                if(col.obj.isA("Point")){
                    Q.state.inc("Points", 10);
                    console.log(Q.state.get("Points"));
                    col.obj.destroy();
                }else if(col.obj.isA("Rock")){
                    //Q.stageScene("endGame",1, { label: "You Died" });
                }else if(col.obj.isA("SeaMonster")){
                 //   Q.stageScene("endGame",1, { label: "You Died" });
                }else if(col.obj.isA("Waves")){
                  //  Q.stageScene("endGame",1, { label: "You Died" });
                }else if(col.obj.isA("Treasure")){
                    col.obj.destroy();
                    //TODO stage finish/end screen
                }
            })
        },
        step : function(dt){
            if(this.p.direction === "left"){
                this.p.flip="y";
                this.play("move_left", 1);
                //this.p.x = this.p.x - this.p.velocity;
            }else if(this.p.direction === "right"){
                this.play("move_right", 1);
                //this.p.x = this.p.x + this.p.velocity;
            }
        }
    });
}