/**
 * Created by Ronald on 12-11-14.
 */

window.onload = function() { // Wait for the window to finish loading

    var Q = window.Q = Quintus()                // Create a new engine instance
        .include("Sprites, Scenes, Input, 2D, Anim, Touch, UI") // Load any needed modules
        .setup("game")                        // Bind Quintus to the canvas with ID "game"
        .controls()                             // Add in default controls (keyboard, buttons)
        .touch();                               // Add in touch support (for the UI)


    //SET TO TRUE FOR DEVELOPMENT ONLY!
    Q.debug = false;
    Q.debugFill = false;

    Q.animations('player', {
        move_left: {frames: [0,1,2,3,4,5,6,7, 8, 9, 10, 11, 12, 13, 14,15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25], rate: 1/10},
        move_right:{frames: [0,1,2,3,4,5,6,7, 8, 9, 10, 11, 12, 13, 14,15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25], rate: 1/10}
    });
//
//    Q.animations('player', {
//        move_left: {frames: [0], rate: 1/10},
//        move_right:{frames: [0],  rate: 1/10}
//    });

    Q.state.reset({Points: 0});

    initializeUI();
    initializeGameObjects();
    initializePlayer();
    initializeLevels();

    var currentPlayer = new Q.Player();

    Q.load("Background.png, empty-tile.png, Schip_varen.png",function() {
      //  Q.sheet("tiles","empty-tile.png", { tilew: 33, tileh: 33});
        Q.sheet("player","Schip_varen.png", { "tilew": 708.5, "tileh": 887,"sx": 0,"sy": 0});
        Q.stageScene("level1");
    });


    var snd = new Audio('./audio/music.mp3');
    snd.play();

    window.castReceiverManager = cast.receiver.CastReceiverManager.getInstance();

    // handler for 'senderconnected' event
    castReceiverManager.onSenderConnected = function(event) {
        console.log('Received Sender Connected event: ' + event.data);
        console.log(window.castReceiverManager.getSender(event.data).userAgent);
    };

    // handler for 'senderdisconnected' event
    castReceiverManager.onSenderDisconnected = function(event) {
        console.log('Received Sender Disconnected event: ' + event.data);
        if (window.castReceiverManager.getSenders().length == 0) {
            window.close();
        }
    };

    window.messageBus =
        window.castReceiverManager.getCastMessageBus(
            'urn:x-cast:cast.whyellow.custom.gameView');

    // handler for the CastMessageBus message event
    window.messageBus.onMessage = function(event) {
        console.log('Message [' + event.senderId + ']: ' + event.data);

        //Parse the JSON string so we can use the data
        var data = JSON.parse(event.data);

        currentPlayer.sensorControls.sensorOutput.x = data.X;
        currentPlayer.sensorControls.sensorOutput.y = data.Y;

        // inform all senders on the CastMessageBus of the incoming message event
        // sender message listener will be invoked
        window.messageBus.send(event.senderId, event.data);
    };
    // initialize the CastReceiverManager with an application status message
    window.castReceiverManager.start({statusText: "Application is starting"});
    console.log('Receiver Manager started');

};