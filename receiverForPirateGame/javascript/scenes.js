/**
 * Created by Ronald on 12-11-14.
 */

var player;

function initializeStartScreen(){

}

function initializeUI(){
    Q.scene('ui', function(stage){
        var container = stage.insert(new Q.UI.Container({
            fill: "gray",
            border: 5,
            shadow: 10,
            shadowColor: "rgba(0,0,0,0.5)",
            y: 50,
            x: Q.width/2
        }));

        var test = Q.UI.Text.extend("Score",{
            init: function(p) {
                this._super({
                    label: "score: 0",
                    color: "white",
                    x: 0,
                    y: 0
                });

                Q.state.on("change.score",this,"score");
            },

            score: function(score) {
                this.p.label = "score: " + score;
            }
        });
    });
}

function insertNewRocksLevel1(stage){
    var counter = 0;
    var i = 0;
    for(i = 0; i <= 130; i++){
        counter++;
        if(i == 20){
            stage.insert(new Q.Rock({x: 800, y:20}));
        }
        if(counter == 20){
            counter = 0;
            stage.insert(new Q.Rock({x: 840 + counter, y:20}));
        }
        if(i == 130){
            counter = 0;
        }
    }
    for(i = 0; i <= 60; i++){
        counter++;
        if(counter == 20){
            counter = 0;
            stage.insert(new Q.Rock({x: 870 + counter, y:220}));
        }
        if(counter == 60){
            counter = 0;
        }
    }
    for(i = 0; i < 120; i++){
        counter++;
        if(counter == 20){
            counter = 0;
            stage.insert(new Q.Rock({x: 200 + (i + counter), y: 130}));
        }
        if(i == 120){
            counter = 0;
        }
    }
    for(i = 0; i < 60; i++){
        counter++;
        if(counter == 20){
            counter = 0;
            stage.insert(new Q.Rock({x: 80 + ( i + counter), y: 180}))
        }
    }

    stage.insert(new Q.Rock({x: 520, y: 360}));
   // stage.insert(new Q.Rock({x: 520, y: 400}));
    stage.insert(new Q.Rock({x: 480, y: 400}));
}

function insertNewSeaMonsterLevel1(stage){

    //Sea monster bottom left
    stage.insert(new Q.SeaMonster({x: 190, y: 440}));
    stage.insert(new Q.SeaMonster({x: 150, y: 440}));
    stage.insert(new Q.SeaMonster({x: 160, y: 480}));

    //Sea monster top center right
    stage.insert(new Q.SeaMonster({x: 515, y: 120}));
    stage.insert(new Q.SeaMonster({x: 540, y: 125}));
    stage.insert(new Q.SeaMonster({x: 540, y: 160}));
}

function insertNewPointsLevel1(stage){
    stage.insert(new Q.Point({x: 400, y:90}));
    stage.insert(new Q.Point({x: 470, y:90}));
    stage.insert(new Q.Point({x: 550, y:100}));
    stage.insert(new Q.Point({x: 630, y:100}));
}

function insertNewTreasureLevel1(stage){
    stage.insert(new Q.Treasure({x:900, y: 450}));
}

function insertNewWavesLevel1(stage){

    var counter = 0;
    var i = 0;
    //Insert waves center right
    for(i = 0; i <= 80; i++){
        counter++;
        if(counter == 20){
            counter = 0;
            stage.insert(new Q.Waves({x: 680 + (i + counter), y:250}));
        }
        if( i == 80){
            counter = 0;
        }
    }

    //Insert waves bottom right
    for(i = 0; i < 80; i++){
        counter++;
        if(counter == 20){
            counter = 0;
            stage.insert(new Q.Waves({x: 600 + ( i + counter ), y: 500}));
        }
    }
}

function initializeLevels(){

    Q.scene('level1', function(stage) {
        stage.insert(new Q.Repeater({ asset: "Background.png",
            speedX: 0,      // Parralax effect
            speedY: 0,      // Parralax effect
            scale: 0.5 }));  // Scale to fit height of window

         player = stage.insert(new Q.Player());

//        stage.add("viewport")
//            .follow(player, {x:true, y:true});
//        stage.viewport.offsetX = 90;
//        stage.viewport.scale = 1.8;

//        insertNewSeaMonsterLevel1(stage);
//        insertNewRocksLevel1(stage);
//        insertNewPointsLevel1(stage);
//        insertNewTreasureLevel1(stage);
//        insertNewWavesLevel1(stage);

//        stage.insert(new Q.Test({x: 500, y: 200}));

        var offSet = 0;
        var secCounter = 0;
        Q.stageScene('ui', 1);
    });

    Q.scene('startScreen', function(stage){
        stage.insert(new Q.Repeater({
            asset: "Background.png",
            speedX: 0,
            speedY: 0
        }));

        var container = stage.insert(new Q.UI.Container({
            x: Q.width/2, y : Q.height/2, fill: "rgba(0,0,0,0.5)"
        }));

        var startScreenLabel = container.insert(new Q.UI.Text({
            x: container.w/2, y: container.h/2
        }));
    });

    Q.scene('endGame', function(stage){
        var box = stage.insert(new Q.UI.Container({
            x: Q.width/2, y: Q.height/2, fill: "rgba(0,0,0,0.5)"
        }));

        var button = box.insert(new Q.UI.Button({ x: 0, y: 0, fill: "#CCCCCC",
            label: "Play Again" }));
        var label = box.insert(new Q.UI.Text({x:10, y: -10 - button.p.h,
            label: stage.options.label }));

        button.on("click", function(){
            Q.stageScene('level1');
        });
    });

    Q.scene('level2', function(stage){

    });
}