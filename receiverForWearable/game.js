/**
 * Created by Ronald on 27-10-14.
 */

var startXPosition;
var startYPosition;
var zPosition;
var scalarComponent;
var mapData;
var score = 0;
var currentLevel = 1;
//var maxScore = mapData.Map.build.length;


function initializeMusic(){
    var music = new Audio("./audio/backgroundMusic.mp3").play();
}

function drawOnScreen(xPosition, yPosition){
    var c=document.getElementById("myCanvas");
    var ctx=c.getContext("2d");
    ctx.fillStyle="#FF0000";
    startXPosition = (startXPosition + xPosition);
    startYPosition = (startYPosition + yPosition);
    ctx.fillRect(startXPosition, startYPosition ,3,3);
}

function rotate(cx, cy, x, y, angle) {
    var radians = (Math.PI / 180) * angle,
        cos = Math.cos(radians),
        sin = Math.sin(radians),
        nx = (cos * (x - cx)) - (sin * (y - cy)) + cx,
        ny = (sin * (x - cx)) + (cos * (y - cy)) + cy;
    return [nx, ny];
}

function drawEndPosition(endPosition){
    var c=document.getElementById("myCanvas");
    var ctx=c.getContext("2d");
    ctx.fillStyle="#FF0000";
    ctx.fillRect(endPosition.xPosition, endPosition.yPosition ,endPosition.height, endPosition.width);
    return true;
}

function checkIfFinished(currentXPosition, currentYPosition, endPosition){
    console.log(endPosition);
    if(currentXPosition > endPosition.xPosition && currentXPosition < (endPosition.xPosition + endPosition.width)
        && currentYPosition > endPosition.yPosition && currentYPosition < (endPosition.yPosition + endPosition.height)){
        console.log("true");
        return true;
    }
    console.log("false");
    return false;
}

/**
 * Loads the next level randomly
 */
function loadNextLevelRandom(){
    var level = Math.floor((Math.random() * 10) + 1 );
    var url = "url(./images/tracingLines"+level+".jpg)";
    clearCanvas();
    $("#myCanvas").css("background-image",url);

    $.getJSON("./levelData/tracingLines"+level+".json", function(data){
            mapData = data;
        });
}

/**
 * Loads the next level
 */
function loadNextLevel(){
    var nextLevel;
    if(currentLevel < 10){
       nextLevel = currentLevel++;
    }
    else{
        currentLevel = 0;
        loadNextLevel();
    }
    clearCanvas();
    var url = "url(./images/tracingLines"+nextLevel+".jpg)";
    $("#myCanvas").css("background-image",url);

    $.getJSON("./levelData/tracingLines"+nextLevel+".json", function(data){
        mapData = data;
    });
}

function clearCanvas(){
    var c=document.getElementById("myCanvas");
    var ctx=c.getContext("2d");
    ctx.clearRect(0, 0, 1000, 1000);
}

/**
 * Requires the build data array from mapData
 */
function buildLevel(buildData){
    for(var i = 0; i < buildData.length; i++){
      //TODO when d3.js is implemented correctly, plot on the coordinates buildData[i][0] = X and buildData[i][1] = Y with D3.js
    }
}
