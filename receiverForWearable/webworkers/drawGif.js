image = new Image();
image.src = "./images/scottpilgrim_multiple.png";
var xpos = 0;
var ypos = 0;
var xSpriteStartPosition;
var ySpriteStartPosition;
var frameHeight = 120;
var frameWidth = 100;
var index = 0;
var numFrames = 8;
var newFrame;

image.onload = function() {
    newFrame = image.width / numFrames;
}

function draw(x, y){
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.clearRect(x, y, 300, 300);
    xSpriteStartPosition = (xSpriteStartPosition + x);
    ySpriteStartPosition = (ySpriteStartPosition + y);
    ctx.drawImage(image, xpos, ypos, frameWidth, frameHeight, xSpriteStartPosition, ySpriteStartPosition, frameWidth, frameHeight);

    console.log(xSpriteStartPosition);
    console.log(ySpriteStartPosition);
//each time around we add the frame size to our xpos, moving along the source image
    xpos += newFrame;
//increase the index so we know which frame of our animation we are currently on
    index += 1;

//if our index is higher than our total number of frames, we're at the end and better start over
    if (index >= numFrames) {
        xpos =0;
        ypos =0;
        index=0;
//if we've gotten to the limit of our source image's width, we need to move down one row of frames

    } else if (xpos + frameWidth > image.width){
        xpos =0;
        ypos =0;
        index=0;
    }
}

function loop(x, y){

    var i = setInterval(function(){
        draw(x, y);
    }, 100);

}