package com.example.ronald.platformer;


import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Ronald on 27-10-2014.
 */
public class Game extends Activity implements SensorEventListener {

    private static final String TAG = Game.class.getSimpleName();

    private SensorManager gameSensorManager;
    private Sensor gameSensor;
    private Sensor mAccelerationSensor;
    private SensorEvent mSensorEvent;
    private SensorEventListener mSensorEventListener;
    private MainScreen mainScreen;
    private final float alpha;
    private float[] gravity;
    private float[] linear_acceleration;
    private float reduceFactor;
    private int heartRate;
    private int score;
    private File jsonData;
    private int[][] points;
    private String[] checks;

    public Game(MainScreen m){
        this.mainScreen = m;
        gravity = new float[3];
        linear_acceleration = new float[3];
        checks = new String[2];
        reduceFactor = 0.8f;
        alpha = 0.8f;
        heartRate = 0;

    }

    public void onCreate(Bundle savedInstances){
        super.onCreate(savedInstances);
//        gameSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
//        gameSensor = gameSensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
//        gameSensorManager.registerListener(this, gameSensor, SensorManager.SENSOR_DELAY_GAME);
    }

    public void startSensor(){
        gameSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        gameSensor = gameSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        gameSensorManager.registerListener(this, gameSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void stopSensor(){
        if(gameSensorManager != null) {
            gameSensorManager.unregisterListener(this);
        }
    }

    public void setJsonData(File jsonFile){
        this.jsonData = jsonFile;
    }

    public void setHeartRate(int heartRate){
        this.heartRate = heartRate;
    }

    public SensorManager getGameSensorManager() {
        return gameSensorManager;
    }

    public void setGameSensorManager(SensorManager gameSensorManager) {
        this.gameSensorManager = gameSensorManager;
    }

    public Sensor getGameSensor() {
        return gameSensor;
    }

    public void setGameSensor(Sensor gameSensor) {
        this.gameSensor = gameSensor;
    }

    public SensorEvent getmSensorEvent() {
        return mSensorEvent;
    }

    public void setmSensorEvent(SensorEvent mSensorEvent) {
        this.mSensorEvent = mSensorEvent;
    }

    public SensorEventListener getmSensorEventListener() {
        return mSensorEventListener;
    }

    public void setmSensorEventListener(SensorEventListener mSensorEventListener) {
        this.mSensorEventListener = mSensorEventListener;
    }

    protected  void onResume(){
        super.onResume();
        gameSensorManager.registerListener(this, gameSensor, SensorManager.SENSOR_DELAY_GAME);
    }

    protected void onPause(){
        super.onPause();
        gameSensorManager.unregisterListener(this);
    }

    public boolean checkDirection(double input){

        String l = "left";
        String r = "right";

        //Go left
        if(input > 0){
            if(checks[0] == null){
                checks[0] = l;
            }else{
                checks[1] = l;
                if(!checks[0].equals(checks[1])){
                    checks[0] = checks[1];
                    return true;
                }
                else{
                    return false;
                }
            }
        }
        //Go right
        else if(input < 0){
            if(checks[0] == null){
                checks[0] = r;
            }else{
                checks[1] = r;
                if(!checks[0].equals(checks[1])){
                    checks[0] = checks[1];
                    return true;
                }
                else{
                    return false;
                }
            }
        }
    return false;

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {


        //Gravity filter
        gravity[0] = alpha * gravity[0] + (1 - alpha) * sensorEvent.values[0];
        gravity[1] = alpha * gravity[1] + (1 - alpha) * sensorEvent.values[1];
        gravity[2] = alpha * gravity[2] + (1 - alpha) * sensorEvent.values[2];

        //Correct the value from the sensor output with the gravity filter
        //Multiplied by reduceFactor to reduce sensitivity
        linear_acceleration[0] = (sensorEvent.values[0] * reduceFactor);// - gravity[0];
        linear_acceleration[1] = (sensorEvent.values[1]) * reduceFactor;
        linear_acceleration[2] = sensorEvent.values[2] - gravity[2];

        //Invert the X axis
//        if(linear_acceleration[1] > 0){
//            linear_acceleration[1] = -linear_acceleration[1];
//        }else if(linear_acceleration[1] < 0){
//            linear_acceleration[1] = Math.abs(linear_acceleration[1]);
//        }

        //Invert the Y axis
        if(linear_acceleration[0] > 0){
            linear_acceleration[0] = -linear_acceleration[0];
        }else if(linear_acceleration[0] < 0){
            linear_acceleration[0] = Math.abs(linear_acceleration[0]);
        }

        //ifEndIsReached(linear_acceleration[1], linear_acceleration[0]);

        JSONObject applicationData = new JSONObject();

        try{
            //Switched the X and the Y axis, now the controls work when the device is held in a horizontal position
            applicationData.put("X", linear_acceleration[1]);
            applicationData.put("Y", linear_acceleration[0]);
           // applicationData.put("heartRate", this.heartRate);
        }catch (JSONException e){
            Log.e(TAG, "Error putting data in JSON object: " + e.getMessage());
        }


        //Check if the direction has changed, if so, we can send a message to the receiver
        if(checkDirection(linear_acceleration[1])){
            mainScreen.sendMessage(applicationData.toString());
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public boolean ifEndIsReached(float x, float y){
        if(x > 0 && x < 200 && y > 100 && y < 200){
            Toast.makeText(getApplicationContext(), R.string.finished_level, Toast.LENGTH_SHORT).show();
            mainScreen.stopSensor();
            return true;
        }
        return false;
    }

    public void createPointsArray(){
        InputStream in = null;
        try{
            in = new FileInputStream(jsonData);
        }
        catch(FileNotFoundException e){
            Log.e(TAG, "Error reading JSON file: " + e);
        }
        JsonReader n = new JsonReader(new InputStreamReader(in));

    }
}
