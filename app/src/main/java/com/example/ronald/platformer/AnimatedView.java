package com.example.ronald.platformer;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Ronald on 10-9-2014.
 */
public class AnimatedView extends ImageView {

    private Context mContext;
    public static int xPosition = -1;
    public static int yPosition = -1;
    private Handler h;
    private final int FRAME_RATE = 30;

    public AnimatedView(Context context, AttributeSet attrs){
        super(context, attrs);

        mContext = context;
        h = new Handler();
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

        private Runnable r = new Runnable() {
        @Override
        public void run() {
            invalidate();
        }
    };

    protected void onDraw(Canvas c) {

//        if (x<0 && y <0) {
//            x = this.getWidth()/2;
//            y = this.getHeight()/2;
//        } else {
//            x += xVelocity;
//            y += yVelocity;
//            if ((x > this.getWidth() - ball.getBitmap().getWidth()) || (x < 0)) {
//                xVelocity = xVelocity*-1;
//            }
//            if ((y > this.getHeight() - ball.getBitmap().getHeight()) || (y < 0)) {
//                yVelocity = yVelocity*-1;
//            }
//        }
    }

//    public static void move(View v){
//
//        switch(v.getId()){
//            case R.id.button_up :
//                yPosition = (yPosition - 10);
//                System.out.println(yPosition);
//                break;
//            case R.id.button_down :
//                yPosition = (yPosition + 10);
//                System.out.println(yPosition);
//                break;
//            case R.id.button_right :
//                xPosition = (xPosition + 10);
//                System.out.println(xPosition);
//                break;
//            case R.id.button_left :
//                xPosition = (xPosition - 10);
//                System.out.println(xPosition);
//                break;
//        }
//    }


}
