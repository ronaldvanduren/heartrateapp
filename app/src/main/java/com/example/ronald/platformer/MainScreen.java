package com.example.ronald.platformer;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.MediaRouteActionProvider;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class MainScreen extends ActionBarActivity {

    private static final String TAG = MainScreen.class.getSimpleName();
    private static final int REQUEST_ENABLE_BT = 1;
    private static final long SCANNING_PERIOD = 10000;
    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    public final static UUID UUID_HEART_RATE_MEASUREMENT =
            UUID.fromString("0000180d-0000-1000-8000-00805f9b34fb");
    public final static String CLIENT_CHARACTERISTIC_CONFIG =
            "00002902-0000-1000-8000-00805f9b34fb";

    public final static String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";

    private Handler mHandler;
    private MediaRouter mediaRouter;
    private MediaRouter.Callback mediaRouterCallBack;
    private MediaRouteSelector mediaRouteSelector;
    private MenuItem mediaRouteMenuItem;
    private CastDevice mSelectedDevice;
    private boolean mWaitingForReconnect;
    private ConnectionFailedListener mConnectionFailedListener;
    private GoogleApiClient mApiClient;
    private ConnectionCallbacks mConnectionCallbacks;
    private Cast.Listener mCastClientListener;
    private Receiver receiverChannel;
    private boolean mApplicationStarted;
    private boolean mConnected;
    private String mSessionId;
    private BluetoothAdapter bluetoothAdapter;
    private Runnable runOnUiThread;
    private LeDeviceListAdapter mLeDeviceListAdapter;
    private BluetoothGatt mBluetoothGatt;
    private boolean mScanning;
    private Server server;
    private BluetoothLeService bluetoothLeService;
    private String mBluetoothDeviceAddress;
    private int mConnectionState;
    private Game deviceGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);


        mConnectionFailedListener = new ConnectionFailedListener();
        mConnectionCallbacks = new ConnectionCallbacks();

        mediaRouter = MediaRouter.getInstance(getApplicationContext());
        // Create a MediaRouteSelector for the type of routes your app supports
        mediaRouteSelector = new MediaRouteSelector.Builder()
                .addControlCategory(
                        CastMediaControlIntent.categoryForCast(getString(R.string.chromecast_app_id))).build();
        // Create a MediaRouter callback for discovery events
        mediaRouterCallBack = new MyMediaRouterCallback();

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);

        //Create Bluetooth adapter for the bluetooth connection
        bluetoothAdapter = bluetoothManager.getAdapter();
        bluetoothLeService = new BluetoothLeService(bluetoothAdapter);
        mLeDeviceListAdapter = new LeDeviceListAdapter();

        //Getting the main thread so we can use the Bluetooth Low Energy API, the methods from this API have to be called from the main thread
        mHandler = new Handler(getApplicationContext().getMainLooper());

        deviceGame = new Game(this);
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean connect(final String address) {
        if (bluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }

        //We have to use a handler to connect to the device since it has to be called from the main thread (This is for the Samsung Galaxy s3/s4)
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                // We want to directly connect to the device, so we are setting the autoConnect
                // parameter to false.
                mBluetoothGatt = device.connectGatt(getApplicationContext(), false, mGattCallback);
            }
        });
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
//        setCharacteristicNotification(mBluetoothGatt.getService(UUID_HEART_RATE_MEASUREMENT).getCharacteristic(UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb")), true);
        return true;
    }

    public void onClick(View v){
        switch(v.getId()){
            case R.id.button_left :
                startSensor();
            break;

            case R.id.button_right :
                stopSensor();
                break;

            case  R.id.button_replay :
            sendMessage("replay");
                break;
        }
    }

    public void startSensor(){
        deviceGame.setGameSensorManager((SensorManager) getSystemService(Context.SENSOR_SERVICE));
        deviceGame.setGameSensor(deviceGame.getGameSensorManager().getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
        deviceGame.getGameSensorManager().registerListener(deviceGame, deviceGame.getGameSensor(), SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void stopSensor(){
        if(deviceGame.getGameSensorManager() != null) {
            deviceGame.getGameSensorManager().unregisterListener(deviceGame);
        }
    }

    public void handleReceiverOutput(String s){
        if(s.equals("finished")){
            stopSensor();
            sendMessage("nextLevel");
        }
    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }

    public UUID findToHeartRateCharacteristic(){
        for(BluetoothGattService bluetoothGattService : getSupportedGattServices()){
            for(BluetoothGattCharacteristic bluetoothGattCharacteristic : bluetoothGattService.getCharacteristics()){
                if(bluetoothGattService.getUuid().equals(UUID_HEART_RATE_MEASUREMENT)){
                    System.out.println("Connected UUIDS: " + bluetoothGattCharacteristic.getUuid());
                    Log.d(TAG, "UUID = " + bluetoothGattService.getUuid());
                    return bluetoothGattService.getUuid();
                }
            }
        }
        Log.d(TAG, "retuns null");
        return null;
    }

    /**
     * Loops through all Gatt services,
     * if one of the Gatt services UUID equals the heart rate service UUID
     * it gets the heart rate characteristic UUID and returns is.
     * ! get(0) is used because the heart rate service has just one characteristic
     * @return characteristic UUID
     */
    public UUID getHeartRateCharacteristic(){
        if(getSupportedGattServices() != null){
            for(BluetoothGattService bluetoothGattService : getSupportedGattServices()){
                if(bluetoothGattService.getUuid().equals(UUID_HEART_RATE_MEASUREMENT)){
                    return bluetoothGattService.getCharacteristics().get(0).getUuid();
                }
            }
        }
        return null;
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled If true, enable notification.  False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {
        if (bluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);

        // This is specific to Heart Rate Measurement.
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                    UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG));
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(descriptor);
    }

    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = ACTION_GATT_CONNECTED;
                mConnectionState = STATE_CONNECTED;
               // broadcastUpdate(intentAction);
                Log.i(TAG, "Connected to GATT server.");
                // Attempts to discover services after successful connection.
                Log.i(TAG, "Attempting to start service discovery:" +
                        mBluetoothGatt.discoverServices());

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;
                Log.i(TAG, "Disconnected from GATT server.");
              //  broadcastUpdate(intentAction);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                setCharacteristicNotification(mBluetoothGatt.getService(UUID_HEART_RATE_MEASUREMENT).getCharacteristic(UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb")), true);
              //  connectToHeartRateCharacteristic();
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            Log.d(TAG, "Read char = " + characteristic);
        }
    };

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        // This is special handling for the Heart Rate Measurement profile.  Data parsing is
        // carried out as per profile specifications:
        // http://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml
        //TODO check if IF statement is correct

        if(characteristic.getUuid() != null){
       // if (characteristic.getUuid().equals(getHeartRateCharacteristic())) {
            int flag = characteristic.getProperties();
            int format = -1;
            if ((flag & 0x01) != 0) {
                format = BluetoothGattCharacteristic.FORMAT_UINT16;
                Log.d(TAG, "Heart rate format UINT16.");
            } else {
                format = BluetoothGattCharacteristic.FORMAT_UINT8;
                Log.d(TAG, "Heart rate format UINT8.");
            }
            final int heartRate = characteristic.getIntValue(format, 1);
            Log.d(TAG, String.format("Received heart rate: %d", heartRate));
            //sendMessage(String.valueOf(heartRate));

            //Set the heartRate in the Data class so it can be added to the JSON object and send to the receiver
            deviceGame.setHeartRate(heartRate);
            intent.putExtra(EXTRA_DATA, String.valueOf(heartRate));
        } else {
            // For all other profiles, writes the data formatted in HEX.
            final byte[] data = characteristic.getValue();
            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for (byte byteChar : data)
                    stringBuilder.append(String.format("%02X ", byteChar));
                intent.putExtra(EXTRA_DATA, new String(data) + "\n" + stringBuilder.toString());
            }
        }
        sendBroadcast(intent);
    }


    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            bluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!bluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            //bluetoothLeService.connect();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bluetoothLeService = null;
        }
    };


    public void scanAndConnect(View v){
        scanLeDevice(true);
    }


    /**
     * Scan for nearby Bluetooth Low Energy devices for a given period of time,
     * when the scanning period expires, the scanning stops.
     * @param enable
     */
    public void scanLeDevice(final boolean enable){
        if(enable){
            mHandler.postDelayed(new Runnable(){
                @Override
                public void run(){
                    mScanning = false;
                    bluetoothAdapter.stopLeScan(leScanCallback);
                };
            }, SCANNING_PERIOD);
            mScanning = true;
            bluetoothAdapter.startLeScan(leScanCallback);
        }
        else{
            mScanning = false;
            bluetoothAdapter.stopLeScan(leScanCallback);
        }
        bluetoothAdapter.startLeScan(leScanCallback);
    }

    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice bluetoothDevice, int i, byte[] bytes) {

            if(bluetoothDevice.getName().equals("Polar H6 33EF2715")){
                if(connect(bluetoothDevice.getAddress())){
                    System.out.println(bluetoothDevice.getName() + " Connected");
                }
                bluetoothAdapter.stopLeScan(leScanCallback);
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mLeDeviceListAdapter.addDevice(bluetoothDevice);
                    mLeDeviceListAdapter.notifyDataSetChanged();
                }
            });
            System.out.println("Device = " + bluetoothDevice.getName());
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_screen, menu);

        //Attach the MediaRouteSelector to the menu item

        //MenuItem
        mediaRouteMenuItem = menu.findItem(R.id.media_route_menu_item);
        MediaRouteActionProvider mediaRouteActionProvider = (MediaRouteActionProvider) MenuItemCompat.getActionProvider(mediaRouteMenuItem);
        mediaRouteActionProvider.setRouteSelector(mediaRouteSelector);
        System.out.println(mediaRouteMenuItem.getTitle());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.media_route_menu_item) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private final BroadcastReceiver btReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent)
        {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
               // updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
              //  updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
              //  clearUI();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the
                // user interface.
             //   displayGattServices(mBluetoothLeService.getSupportedGattServices());
                System.out.println("Gatt Service: " + action);
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
              //  displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                sendMessage(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
               //changeColor(Integer.parseInt(intent.getStringExtra(BluetoothLeService.EXTRA_DATA)));
            }
        }
    };

    public void closeGattService(){
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();

        mediaRouter.addCallback(mediaRouteSelector, mediaRouterCallBack,
                MediaRouter.CALLBACK_FLAG_REQUEST_DISCOVERY);
    }

    protected void onStart(){
        super.onStart();

        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        //Starting the web server on which the receiver is hosted.
        try {
            server = new Server(Integer.parseInt(getString(R.string.server_port)), deviceGame);
            server.start();
            System.out.println("SERVER STARTED");
            System.out.println("IPV4 " + Utils.getIPAddress(true));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void onDestroy(){
        super.onDestroy();
        unbindService(mServiceConnection);
        closeGattService();

        /*
         * Temporarily disabled!
        if(bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.disable();
        }*/

        server.stop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {
            mediaRouter.removeCallback(mediaRouterCallBack);
        }
        stopSensor();
    }

    public void sendMessage(String message) {
        if (mApiClient != null && receiverChannel != null) {
            try {
                Cast.CastApi.sendMessage(mApiClient,
                        receiverChannel.getNameSpace(), message)
                        .setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status result) {
                                if (!result.isSuccess()) {
                                    Log.e(TAG, "Sending message failed");
                                    System.out.println("Sending message failed");
                                }
                                else{
                                    System.out.println("Success sending message");
                                }
                            }
                        });
            } catch (Exception e) {
                Log.e(TAG, "Exception while sending message", e);
            }
        } else {
//            Toast.makeText(MainScreen.this, message, Toast.LENGTH_SHORT)
//                    .show();
        }
    }

    private void teardown() {
        Log.d(TAG, "teardown");
        if (mApiClient != null) {
            if (mApplicationStarted) {
                if (mApiClient.isConnected() || mApiClient.isConnecting()) {
                    try {
                        Cast.CastApi.stopApplication(mApiClient, mSessionId);
                        if (receiverChannel != null) {
                            Cast.CastApi.removeMessageReceivedCallbacks(
                                    mApiClient,
                                    receiverChannel.getNameSpace());
                            receiverChannel = null;
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Exception while removing channel", e);
                    }
                    mApiClient.disconnect();
                }
                mApplicationStarted = false;
            }
            mApiClient = null;
        }
        mSelectedDevice = null;
        mWaitingForReconnect = false;
        mSessionId = null;
    }

    private void launchReceiver() {
        try {
            mCastClientListener = new Cast.Listener() {
                @Override
                public void onApplicationStatusChanged() {
                    if (mApiClient != null) {
                        Log.d(TAG, "onApplicationStatusChanged: "
                                + Cast.CastApi.getApplicationStatus(mApiClient));
                    }
                }

                @Override
                public void onVolumeChanged() {
                    if (mApiClient != null) {
                        Log.d(TAG, "onVolumeChanged: " + Cast.CastApi.getVolume(mApiClient));
                    }
                }

                @Override
                public void onApplicationDisconnected(int errorCode) {
                    teardown();
                }
            };

            Cast.CastOptions.Builder apiOptionsBuilder = Cast.CastOptions
                    .builder(mSelectedDevice, mCastClientListener);

            mApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Cast.API, apiOptionsBuilder.build())
                    .addConnectionCallbacks(mConnectionCallbacks)
                    .addOnConnectionFailedListener(mConnectionFailedListener)
                    .build();

            mApiClient.connect();

            System.out.println("Connected to GoogleApiClient");
        } catch (Exception e) {
            System.out.println("Failed to launch receiver, error: " + e);
        }
    }

    private class MyMediaRouterCallback extends MediaRouter.Callback {

        @Override
        public void onRouteSelected(MediaRouter router, MediaRouter.RouteInfo info) {
            Log.d(TAG, "onRouteSelected");
            // Handle route selection.
            mSelectedDevice = CastDevice.getFromBundle(info.getExtras());
            launchReceiver();
        }

        @Override
        public void onRouteUnselected(MediaRouter router, MediaRouter.RouteInfo info) {
            Log.d(TAG, "onRouteUnselected: info=" + info);
            mSelectedDevice = null;
        }
    }

    private class ConnectionCallbacks implements
            GoogleApiClient.ConnectionCallbacks {
        @Override
        public void onConnected(Bundle connectionHint) {
            Log.d(TAG, "onConnected");

            if (mApiClient == null) {
                // We got disconnected while this runnable was pending
                // execution.
                return;
            }

            try {
                if (mWaitingForReconnect) {
                    mWaitingForReconnect = false;

                    // Check if the receiver app is still running
                    if ((connectionHint != null)
                            && connectionHint
                            .getBoolean(Cast.EXTRA_APP_NO_LONGER_RUNNING)) {
                        Log.d(TAG, "App  is no longer running");
                        teardown();
                    } else {
                        // Re-create the custom message channel
                        try {
                            Cast.CastApi.setMessageReceivedCallbacks(
                                    mApiClient,
                                    receiverChannel.getNameSpace(),
                                    receiverChannel);
                        } catch (IOException e) {
                            Log.e(TAG, "Exception while creating channel", e);
                        }
                    }
                } else {
                    // Launch the receiver app
                    Cast.CastApi
                            .launchApplication(mApiClient,
                                    getString(R.string.chromecast_app_id), false)
                            .setResultCallback(
                                    new ResultCallback<Cast.ApplicationConnectionResult>() {
                                        @Override
                                        public void onResult(
                                                Cast.ApplicationConnectionResult result) {
                                            Status status = result.getStatus();
                                            Log.d(TAG,
                                                    "ApplicationConnectionResultCallback.onResult: statusCode"
                                                            + status.getStatusCode());
                                            if (status.isSuccess()) {
                                                ApplicationMetadata applicationMetadata = result
                                                        .getApplicationMetadata();
                                                mSessionId = result
                                                        .getSessionId();
                                                String applicationStatus = result
                                                        .getApplicationStatus();
                                                boolean wasLaunched = result
                                                        .getWasLaunched();
                                                Log.d(TAG,
                                                        "application name: "
                                                                + applicationMetadata
                                                                .getName()
                                                                + ", status: "
                                                                + applicationStatus
                                                                + ", sessionId: "
                                                                + mSessionId
                                                                + ", wasLaunched: "
                                                                + wasLaunched);
                                                mApplicationStarted = true;

                                                // Create the custom message
                                                // channel
                                                receiverChannel = new Receiver();
                                                try {
                                                    Cast.CastApi
                                                            .setMessageReceivedCallbacks(
                                                                    mApiClient,
                                                                    receiverChannel
                                                                            .getNameSpace(),
                                                                    receiverChannel);
                                                } catch (IOException e) {
                                                    Log.e(TAG,
                                                            "Exception while creating channel",
                                                            e);
                                                }

                                                // set the initial instructions
                                                // on the receiver
                                            } else {
                                                Log.e(TAG,
                                                        "application could not launch");
                                                teardown();
                                            }
                                        }
                                    });
                }
            } catch (Exception e) {
                Log.e(TAG, "Failed to launch application", e);
            }
        }

        @Override
        public void onConnectionSuspended(int cause) {
            Log.d(TAG, "onConnectionSuspended");
            mWaitingForReconnect = true;
        }
    }


    private class ConnectionFailedListener implements
            GoogleApiClient.OnConnectionFailedListener {
        @Override
        public void onConnectionFailed(ConnectionResult result) {
            teardown();
        }
    }

    class Receiver implements Cast.MessageReceivedCallback {

        public String getNameSpace() {
            return "urn:x-cast:cast.whyellow.custom.gameView";
        }

        @Override
        public void onMessageReceived(CastDevice castDevice, String namespace, String message) {
            Log.d(TAG, "onMessageReceived: " + message);
            handleReceiverOutput(message);
        }
    }

    private class LeDeviceListAdapter extends BaseAdapter {

        private ArrayList<BluetoothDevice> mLeDevices;

        public LeDeviceListAdapter(){
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
        }

        public void addDevice(BluetoothDevice d){
            if(!mLeDevices.contains(d)){
                mLeDevices.add(d);
            }
        }

        public BluetoothDevice getBluetoothDeviceByPosition(int position){
            return mLeDevices.get(position);
        }

        public BluetoothDevice getBluetoothDeviceByDevice(BluetoothDevice d){
            Iterator it = mLeDevices.iterator();
            while(it.hasNext()){
                if(it.next().equals(d)){
                    return d;
                }
            }
            return null;
        }

        public void clear(){
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            return null;
        }
    }




}
